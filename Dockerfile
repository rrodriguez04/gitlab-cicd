# GET SDK IMAGE 
FROM mcr.microsoft.com/dotnet/framework/sdk:4.8 AS build
WORKDIR /app

# copy everything else and build app
COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release - o out

FROM mcr.microsoft.com/dotnet/framework/sdk:4.8
WORKDIR /app
EXPOSE 80
COPY --from=build-env /app/out .
ENTRYPOINT [ "dotnet","GITLABPIPELINE.dll" ]